<?php

add_filter('the_generator', 'remove_wp_version_wp_head_feed');
function remove_wp_version_wp_head_feed()
{
  return false;
}

add_filter('admin_footer_text', 'change_admin_footer');
function change_admin_footer() {
  $footer_text = [
    'Thank you for your creativity with <a href="http://wordpress.org">WordPress</a>',
    'Developed by <a href="https://gitlab.com/tretyakoffc" target="_blank">Constantine Tretyakoff</a>',
  ];
  return implode(' &bull; ', $footer_text);
}
